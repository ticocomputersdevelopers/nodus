
<div id="main-content" class="kupci-page">
    @if(Session::has('message'))
    <script>
        alertify.success('{{ Session::get('message') }}');
    </script>
    @endif
    
    <div class="row"> 
        <div class="large-2 medium-3 small-12 columns">
            <a class="m-subnav__link JS-nazad" href="{{ AdminOptions::base_url() }}admin/partner/{{$partner_id}}/korisnici">
                <div class="m-subnav__link__icon">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </div>
                <div class="m-subnav__link__text">
                    {{ AdminLanguage::transAdmin('Nazad') }}
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <h3 class="title-med">
                Korisnici partnera - {{ AdminPartneri::partner($partner_id)->naziv }} 
             </h3>
             <form action="{{AdminOptions::base_url()}}/admin/partner/user_edit" method="post">
                <input type="hidden" name="partner_korisnik_id" value="{{ $partnerKorisnik->partner_korisnik_id }}">
                <input type="hidden" name="partner_id" value="{{ $partner_id }}">
                <div class="flat-box"> 
                    <div class="row">  
                        <div class="columns medium-4"> 
                            <label class="title-med">Podaci za dostavu</label>

                            <label for="naziv">Naziv*</label>
                            <input class="form-control" id="naziv" name="naziv" type="text" value="{{ !is_null(Input::old('naziv')) ? Input::old('naziv') : $partnerKorisnik->naziv}}">
                            <div class="error red-dot-error">{{ $errors->first('naziv') }}</div> 

                            <label for="adresa">Adresa*</label>
                            <input class="form-control" id="adresa" name="adresa" type="text" value="{{ !is_null(Input::old('adresa')) ? Input::old('adresa') : $partnerKorisnik->adresa}}">
                            <div class="error red-dot-error">{{ $errors->first('adresa') }}</div> 

                            <label for="mesto">Mesto*</label>
                            <input class="form-control" id="mesto" name="mesto" type="text" value="{{ !is_null(Input::old('mesto')) ? Input::old('mesto') : $partnerKorisnik->mesto}}">
                            <div class="error red-dot-error">{{ $errors->first('mesto') }}</div> 

                            <label for="telefon">Telefon*</label>
                            <input class="form-control" id="telefon" name="telefon" type="text" value="{{ !is_null(Input::old('telefon')) ? Input::old('telefon') : $partnerKorisnik->telefon}}">
                            <div class="error red-dot-error">{{ $errors->first('telefon') }}</div>

                            <label for="email">E-mail*</label>
                            <input class="form-control" id="email" name="email" type="text" value="{{ !is_null(Input::old('email')) ? Input::old('email') : $partnerKorisnik->email}}">
                            <div class="error red-dot-error">{{ $errors->first('email') }}</div>
                        </div>  

                        <div class="columns medium-4"> 
                            <label class="title-med">Pristupni podaci</label> 
                            <label for="korisnicko_ime">Korisničko ime*</label>
                            <input class="form-control" id="korisnicko_ime" name="korisnicko_ime" type="text" value="{{ !is_null(Input::old('korisnicko_ime')) ? Input::old('korisnicko_ime') : $partnerKorisnik->korisnicko_ime}}">
                            <div class="error red-dot-error">{{ $errors->first('korisnicko_ime') }}</div>

                            <label for="lozinka">Lozinka*</label>
                            <input class="form-control" id="lozinka" name="lozinka" type="text" value="{{ !is_null(Input::old('lozinka')) ? Input::old('lozinka') : $partnerKorisnik->lozinka}}">
                            <div class="error red-dot-error">{{ $errors->first('lozinka') }}</div>

                            <label for="aktivan">Aktivan*</label>
                            <select class="form-control" id="aktivan" name="aktivan">
                                <option value="1">Da</option>
                                <option value="0" {{ ((!is_null(Input::old('aktivan')) ? Input::old('aktivan') : $partnerKorisnik->aktivan) == 0) ? 'selected' : '' }}>Ne</option>
                            </select>
                            <div class="error red-dot-error">{{ $errors->first('aktivan') }}</div> 
                        </div> 
                    </div> 

                    <div class="row"> 
                        <div class="columns small-8 center btn-container"> 
                            <button class="btn btn-primary save-it-btn">Sačuvaj</button>
                        
                            @if($partnerKorisnik->partner_korisnik_id > 0) 
                                <a href="{{ AdminOptions::base_url() }}admin/partner/{{$partner_id}}/korisnik/{{ $partnerKorisnik->partner_korisnik_id }}/delete" class="submit admin-login btn btn-danger">Obriši</a>
                            @endif
                        </div>
                    </div>
                </div> 
            </form>
        </div>
    </div>
</div>