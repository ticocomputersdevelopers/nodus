	
<div class="m-tabs clearfix">
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'grupe' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/grupe/0">{{ AdminLanguage::transAdmin('GRUPE') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'proizvodjac' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/proizvodjac/0">{{ AdminLanguage::transAdmin('PROIZVOĐAČI') }} </a></div>
	@endif
<!-- 	
@if(Admin_model::check_admin(array(1800)))
	<div class="m-tabs__tab{{ $strana == 'mesto' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/mesto">MESTA </a></div>
	@endif -->
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'tip_artikla' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/tip/0">{{ AdminLanguage::transAdmin('TIPOVI') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'labela_artikla' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/labela/0">{{ AdminLanguage::transAdmin('LABELE') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'jedinica_mere' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/jedinica-mere/0">{{ AdminLanguage::transAdmin('JEDINICE MERE') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'poreske_stope' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/poreske-stope">{{ AdminLanguage::transAdmin('PORESKE STOPE') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'stanje_artikla' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/stanje-artikla">{{ AdminLanguage::transAdmin('STANJA ARTIKLA') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'status_narudzbine' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/status_narudzbine">{{ AdminLanguage::transAdmin('STATUS NARUDŽBINE') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1]))
	<div class="m-tabs__tab{{ $strana == 'konfigurator' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/konfigurator">{{ AdminLanguage::transAdmin('KONFIGURATORI') }} </a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1])) 
		@if(in_array(AdminB2BOptions::web_options(130),array(0,1)) && Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
		<div class="m-tabs__tab{{ $strana == 'komentari' || $strana == 'komentar' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/komentari">{{ AdminLanguage::transAdmin('KOMENTARI') }} </a></div>
		@endif
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1]))
	<div class="m-tabs__tab{{ $strana == 'osobine' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/osobine/0">{{ AdminLanguage::transAdmin('OSOBINE') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'nacin_placanja' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/nacin_placanja/0">{{ AdminLanguage::transAdmin('NAČIN PLAĆANJA') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'nacin_isporuke' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/nacin_isporuke/0">{{ AdminLanguage::transAdmin('NAČIN ISPORUKE') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'troskovi_isporuke' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/troskovi-isporuke">{{ AdminLanguage::transAdmin('TROŠKOVI ISPORUKE') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'kurirska_sluzba' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/kurirska_sluzba">{{ AdminLanguage::transAdmin('KURIRSKA SLUŽBA') }}</a></div>
	@endif
	
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'kurs' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/kurs/2">{{ AdminLanguage::transAdmin('UNOS KURSA') }}</a></div>
	@endif
 
	@if((AdminOptions::gnrl_options(3042) == 1) AND Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'definisane_marze' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/definisane-marze">{{ AdminLanguage::transAdmin('DEFINISANE MARŽE') }}</a></div>
	@endif
	
	@if((AdminOptions::web_options(313) == 1) AND Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1]))
	<div class="m-tabs__tab{{ $strana == 'kupovina_na_rate' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/kupovina_na_rate/0">KUPOVINA NA RATE</a></div>
	@endif
	
	@if((AdminOptions::web_options(314) == 1) AND Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1]))
	<div class="m-tabs__tab{{ $strana == 'bodovi' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/bodovi">{{ AdminLanguage::transAdmin('BODOVI') }}</a></div>
	@endif
	
	@if((AdminOptions::web_options(318) == 1) AND Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) AND in_array(AdminB2BOptions::web_options(130),[0,1]))
	<div class="m-tabs__tab{{ $strana == 'vauceri' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/vauceri">VAUČERI</a></div>
	@endif
	 
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'katalog' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/katalog/0">KATALOG</a></div>
	@endif
	 
	@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
	<div class="m-tabs__tab{{ $strana == 'ankete' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminOptions::base_url() }}admin/ankete/0">{{ AdminLanguage::transAdmin('ANKETE') }}</a></div>
	@endif

</div>