<div id="main-content">
	@include('admin/partials/product-tabs')

	<div class="m-tabs clearfix"> 
		@if(AdminOptions::checkB2C())
			@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
				<div class="m-tabs__tab{{ $strana=='product_akcija' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija/{{ $roba_id }}">{{ AdminLanguage::transAdmin('B2C AKCIJA') }} </a></div>
			@endif
		@endif
		 
		@if(AdminOptions::checkB2B())
			@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
				<div class="m-tabs__tab{{ $strana=='product_akcija_b2b' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/product_akcija_b2b/{{ $roba_id }}">{{ AdminLanguage::transAdmin('B2B AKCIJA') }}</a></div>
			@endif 
		@endif
	</div>

	<div class="columns medium-7 medium-centered">	
		<form method="POST" action="{{AdminOptions::base_url()}}admin/akcija_edit">
			<div class="flat-box">
				<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">

				<div class="row">
					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('B2C akcija aktivna') }}</label>
						<select name="akcija_flag_primeni" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@if(Input::old('akcija_flag_primeni') ? Input::old('akcija_flag_primeni') : $akcija_primeni)
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
						</select>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Redni broj') }}</label>
						<input type="text" name="akcija_redni_broj" class="{{ $errors->first('akcija_redni_broj') ? 'error' : '' }}" value="{{ Input::old('akcija_redni_broj') ? Input::old('akcija_redni_broj') : $redni_broj }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Datum od') }}</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_od') ? Input::old('datum_akcije_od') : $datum_akcije_od }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
						
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
							@endif
						</div>
					</div>

					<div class="column medium-3 field-group">
						<label>{{ AdminLanguage::transAdmin('Datum do') }}</label>
						<div class="relative"> 
							<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" value="{{ Input::old('datum_akcije_do') ? Input::old('datum_akcije_do') : $datum_akcije_do }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
							<span id="datum_do_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="flat-box"> 
				<div class="row">
					<div class="column medium-4 field-group">
						<label>{{ 'Maloprodajna cena' }}</label>
						<input class="<?php if($errors->first('web_cena')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="web_cena" value="{{ Input::old('web_cena') ? Input::old('web_cena') : $mpcena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-4 field-group">
						<label>{{ AdminLanguage::transAdmin('Akcijski popust (%)') }}</label>
						<input class="<?php if($errors->first('akcija_popust')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="akcija_popust" value="{{ Input::old('akcija_popust') ? Input::old('akcija_popust') : $akcija_popust }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-4 field-group">
						<label>{{ AdminLanguage::transAdmin('Akcijska cena') }}</label>
						<input class="<?php if($errors->first('akcijska_cena')){ echo 'error'; }else{ if(Session::get('message')){ echo 'error'; } } ?>" type="text" name="akcijska_cena" value="{{ Input::old('akcijska_cena') ? Input::old('akcijska_cena') : $akcijska_cena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
				</div> 
		  
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) 
				<div class="btn-container center">
					<button type="submit" class="setting-button btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
				</div>	 
				@endif 
			</div>
		</form>	
	</div>
</div>
