<?php $facebook_pixel = Options::facebookPixel();
$korpa_id=Session::get('b2c_korpa');
?>
@if($facebook_pixel and $facebook_pixel->int_data == 1 and $facebook_pixel->str_data and $facebook_pixel->str_data != '')

<!-- Facebook Pixel Code -->
<script>
 !function(f,b,e,v,n,t,s)
 {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 n.callMethod.apply(n,arguments):n.queue.push(arguments)};
 if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
 n.queue=[];t=b.createElement(e);t.async=!0;
 t.src=v;s=b.getElementsByTagName(e)[0];
 s.parentNode.insertBefore(t,s)}(window, document,'script',
 'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '{{ $facebook_pixel->str_data }}');
 @if($strana == 'artikal')
 fbq('track', 'ViewContent', { content_ids:{{$roba_id}},content_type:'product', value: {{Product::get_price($roba_id)}}, currency: 'RSD' });
 @endif
 @if(Session::has('success_add_to_cart'))
 fbq('track', 'AddToCart', { content_ids:{{$roba_id}},content_type:'product', value: {{Product::get_price($roba_id)}}, currency: 'RSD' });
 @endif
 @if($strana == 'order')
 fbq('track', 'Purchase', {value: {{Order::narudzbina_ukupno($web_b2c_narudzbina_id)}}, currency: 'RSD'});
 @endif
 fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id={{ $facebook_pixel->str_data }}&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --> 
@endif
