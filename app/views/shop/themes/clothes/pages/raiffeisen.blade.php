@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')


	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nakon potvrde vrši se preusmeravanje na sajt Raiffaisen Banke, gde se na zaštićenoj stranici realizuje proces plaćanja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
		{{ Language::trans('Uslovi korišćenja') }}</a>
		

        		<!-- <form action="https://ecg.test.upc.ua/rbrs/enter" method="post" id="rba-form"> -->
        		<form action="https://ecommerce.raiffeisenbank.rs/rbrs/enter" method="post" id="rba-form">

		 		<button type="submit" class="button" id="JSRaifSubmit" disabled>{{ Language::trans('Završi plaćanje karticom') }}</button>

				<input type="hidden" value="1" name="Version">
				<input type="hidden" value="{{ $MerchantID }}" name="MerchantID">
				<input type="hidden" value="{{ $TerminalID }}" name="TerminalID">
				<input type="hidden" value="{{ $TotalAmount }}" name="TotalAmount">
				<input type="hidden" value="{{ $CurrencyID }}" name="Currency">
				<input type="hidden" value="sr" name="locale">
				<input type="hidden" value="{{ $PurchaseTime }}" name="PurchaseTime">
				<input type="hidden" value="{{ $OrderID }}" name="OrderID">
				<input type="hidden" value="" name="PurchaseDesc">
				<input type="hidden" value="{{ $Signature }}" name="Signature">
	
				
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSRaifSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSRaifSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection
