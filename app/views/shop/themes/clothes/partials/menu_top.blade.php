@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div id="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container-fluid text-right"> 
        <div class="top-menu row">

            <div class="col-md-12 col-sm-12 col-xs-12 no-padding"> 

                <!-- LOGIN AND REGISTRATION -->
                @if(Session::has('b2c_kupac'))
                    <div class="flex log-user">

                        <span class="JSbroj_wish far fa-heart">{{ Cart::broj_wish() }}</span> 
                          
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">{{ WebKupac::get_user_name() }}</a> 

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">{{ WebKupac::get_company_name() }}</a> 
                        &nbsp;
                        <a id="logout-button" href="{{Options::base_url()}}logout" title="{{ Language::trans('Odjavi se') }}">
                            <i class="fas fa-sign-out-alt"></i>
                        </a>
                    </div>
                    
                    @else 
                    
                    <div class="dropdown inline-block">
                        <ul class="hidden-small JStoggle-content"> 
                            <li>
                                <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijava') }}</a>
                            </li>

                            <li>
                                <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Registracija') }}</a>
                            </li>
                        </ul>
                    </div> 
                @endif 

            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 hidden">     
                
                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block color-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

            </div>

            <div class="col-md-1 col-sm-1 col-xs-2 text-center"> 
                @if(Options::checkB2B())
                <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                @endif 
            </div>   
 
        </div> 
    </div>
</div>


