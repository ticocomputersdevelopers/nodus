 
<div class="header-cart-container inline-block relative">  
	
	<a class="header-cart flex text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}">
		<span class="header-cart-info hidden-sm hidden-xs">(<span class="JScart-count-num">{{ Cart::broj_cart() }}</span> <span class="JScart-count-label"></span>)</span>
		<span class="header-cart-info hidden-sm hidden-xs"><span class="cart-price-count">{{ Cart::cena(Cart::cart_ukupno()) }}</span></span>
		<i class="fas fa-shopping-cart"></i>		
		<span class="JScart_num badge hidden-to-sm"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs text-left">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div> 