@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product') 
@endif

<div id="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container-fluid text-right"> 
        <div class="top-menu row">

            <div class="col-md-12 col-sm-12 col-xs-12">     
                
                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

            </div>

            <div class="col-md-1 col-sm-1 col-xs-2 text-center"> 
                @if(Options::checkB2B())
                <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                @endif 
            </div>   
 
        </div> 
    </div>
</div>


