<div class="header-cart-container relative inline-block">  
	
	<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}">
		<span class="header-cart-info">({{ Cart::broj_cart() }} {{ Language::trans('Artikli') }})</span>
		<span class="header-cart-info">{{ Cart::cena(Cart::cart_ukupno()) }}</span>
		<svg id="i-cart" class="shopIconUser itemCartShop cart-me" viewBox="0 0 32 32" width="26" height="26" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.4">
            <path d="M6 6 L30 6 27 19 9 19 M27 23 L10 23 5 2 2 2"></path>
            <circle cx="25" cy="27" r="2"></circle>
            <circle cx="12" cy="27" r="2"></circle>
		</svg>
		<span class="JScart_num badge hidden"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div>
