
@if(Session::has('message'))
<script>
	alertify.success('{{ Session::get('message') }}');
</script>
@endif

<section id="main-content">

	@include('crm/partials/crm_tabs')

	<div class="row">
		<div class="columns medium-12">  
			<div class="flat-box">
				<div class="row"> 
					<div class="columns medium-12 no-padd">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('CRM partneri') }}</h3>
					</div>
		
					<div class="column medium-2 no-padd">
						<label class="no-margin">&nbsp; {{ AdminLanguage::transAdmin('Ukupno') }}: <b>{{$count}}</b></label>
					</div>

	 				<div class="column medium-3">
	 					<a href="" class="btn btn-create btn-small" data-reveal-id="add-partner"> {{ AdminLanguage::transAdmin('Novi partner') }}</a>		
						<a class="btn btn-secondary btn-small" href="{{ AdminB2BOptions::base_url()}}admin/b2b/kupci_partneri/partneri" target='_blank'>{{ AdminLanguage::transAdmin('Svi partneri') }}</a>	
	 				</div>

					<div class="column medium-7 no-padd">
						<!-- SEARCH -->
						<div class="m-input-and-button">
							<input type="text" name="search" value="" placeholder={{ AdminLanguage::transAdmin('Pretraga...') }} >
							<button id="JSCRMSearch" class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Pretraga') }}</button>

							<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}crm/crm_partneri">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</div>  
					</div>
	 			</div>
	 			<br>
				<div class="table-scroll">
					<table class="fixed-table-header">
						<thead class="table-head">
							<tr>
								<th>{{ AdminLanguage::transAdmin('Partner naziv') }}</th>
								<th>{{ AdminLanguage::transAdmin('Adresa') }}</th>  
								<th>{{ AdminLanguage::transAdmin('Mesto/grad') }}</th> 
								<th>{{ AdminLanguage::transAdmin('Naziv delatnosti') }}</th> 		
								<th>{{ AdminLanguage::transAdmin('Email') }}</th>
								<!-- <th>{{ AdminLanguage::transAdmin('Komercijalista') }}</th> -->
								<th>{{ AdminLanguage::transAdmin('Kontakt osoba') }}</th>
								<th>{{ AdminLanguage::transAdmin('Telefon') }}</th>
								<th>{{ AdminLanguage::transAdmin('Pib') }}</th>
								<th>{{ AdminLanguage::transAdmin('Matični broj') }}</th>   
								<th></th>
								<th></th>    
							</tr>
						</thead>

						<tbody> 

							@foreach($partneri as $row)
							<tr>
								<form action="{{AdminOptions::base_url()}}crm/crm_partneri_save/{{$row->partner_id}}" method="post" autocomplete="false">
									<input type='hidden' value="{{ $row->partner_id }}" name='partner_id'>
									<td><input type='text' value="{{ $row->naziv }}" name='naziv'> </td>
									<td><input type='text' value="{{ $row->adresa }}" name='adresa'> </td>
									<td><input type='text' value="{{ $row->mesto }}" name='mesto'> </td>
									<td><input type='text' value="{{ $row->delatnost_naziv }}" name='delatnost_naziv'> </td>	
									<td><input type='text' value="{{ $row->mail }}" name='mail'> </td>
									<!-- <td><input type='text' value="{{ $row->komercijalista }}" name='komercijalista'> </td> -->
									<td><input type='text' value="{{ $row->kontakt_osoba }}" name='kontakt_osoba'> </td>
									<td><input type='text' value="{{ $row->telefon }}" name='telefon'> </td>
									<td><input type='text' value="{{ $row->pib }}" name='pib'> </td>
									<td><input type='text' value="{{ $row->broj_maticni }}" name='broj_maticni'> </td>
									<td><button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i>
									</button></td>
									<td><a href="{{ AdminOptions::base_url() }}crm/crm_partneri/{{$row->partner_id}}/partner_obrisi" onclick="return confirm('Jeste li sigurni da želite da obrišete partnera?')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši partnera') }}"><i class="fa fa-times red"></i></a> 
									</td>
								</form>
							</tr>
							@endforeach

						</tbody>
					</table>  
				</div>

				{{ Paginator::make($partneri,count($partneri),5)->links() }}				
			</div>
		</div>
	</div> 
	<!-- modal novi partner -->
	<div id="add-partner" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<br>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodaj novog partnera') }}</h4>
		<div class="row">
	        <div class="medium-10 medium-offset-1">
				<form class="row" action="{{AdminOptions::base_url()}}crm/crm_partneri_save" method="post"  autocomplete="false">
					<div class="column medium-6">
						<input type='hidden' value="1" name='drzava_id'>
						<div class="flat-box padding-h-8">
							<div>
								<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
								<input class="{{ $errors->first('naziv') ? 'error' : '' }}"  name="naziv" value="" type="text" >
								<div class="error">{{ $errors->first('naziv') }}</div>
							</div>

							<div>
								<label>{{ AdminLanguage::transAdmin('Puni naziv') }}</label>
								<input class="{{ $errors->first('naziv_puni') ? 'error' : '' }}" id="naziv_puni" name="naziv_puni" value="" type="text" >
							</div>

							<div>
								<label>{{ AdminLanguage::transAdmin('Broj') }}</label>
								<input class="{{ $errors->first('broj') ? 'error' : '' }}" class="form-control" name="broj" type="text" value="" >
								<div class="error">{{ $errors->first('broj') }}</div>
							</div>

							<div class="row"> 
								<div class="columns medium-6 padding-r-5">
									<label>{{ AdminLanguage::transAdmin('Adresa') }}</label>
									<input class="{{ $errors->first('adresa') ? 'error' : '' }}" name="adresa" value="" type="text" >
									<div class="error">{{ $errors->first('adresa') }}</div>
								</div>

								<div class="columns medium-6 padding-l-5">
									<label> {{ AdminLanguage::transAdmin('Mesto/Grad') }}</label>
									<input class="{{ $errors->first('mesto') ? 'error' : '' }}" type="text" name="mesto" value="">
									<div class="error" >{{ $errors->first('mesto') }}</div>
								</div>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Email') }}</label> 
								<input class="{{ $errors->first('mail') ? 'error' : '' }}" id="mail" autocomplete="off" name="mail" value="" type="email" > 
								<div class="error">{{ $errors->first('mail') }}</div>	
							</div>

							<div>
								<label>{{ AdminLanguage::transAdmin('Naziv delatnosti') }}</label>
								<input class="{{ $errors->first('delatnost_naziv') ? 'error' : '' }}"  name="delatnost_naziv" value="" type="text" >
							</div> 
						</div> 
					</div>
	 
					<div class="column medium-6">
						<div class="flat-box padding-h-8">

							<div>
								<label>{{ AdminLanguage::transAdmin('Kontakt Osoba') }}</label>
								<input type="text" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" name="kontakt_osoba" value="">
								<div class="error">{{ $errors->first('kontakt_osoba') }}</div>
							</div>
						
							<div class="">
								<label>{{ AdminLanguage::transAdmin('Telefon') }}</label>
								<input type="text" class="{{ $errors->first('telefon') ? 'error' : '' }}" name="telefon" value="" >
								<div class="error">{{ $errors->first('telefon') }}</div>
							</div>

							<div class="">
								<label>{{ AdminLanguage::transAdmin('Pib') }}</label>
								<input class="{{ $errors->first('pib') ? 'error' : '' }}" name="pib" value="" type="text" >
								<div class="error">{{ $errors->first('pib') }}</div>
							</div>

							<div>
								<label>{{ AdminLanguage::transAdmin('Račun') }}</label>
								<input class="{{ $errors->first('racun') ? 'error' : '' }}" autocomplete="off" name="racun" value="" type="text" >
								<div class="error">{{ $errors->first('racun') }}</div>
							</div>

							<div> 
								<div class="">
									<label>{{ AdminLanguage::transAdmin('Matični broj') }}</label>
									<input class="{{ $errors->first('broj_maticni') ? 'error' : '' }}"  name="broj_maticni" value="" type="text" >
									<div class="error">{{ $errors->first('broj_maticni') }}</div>
								</div>
							</div>
						</div> 

	                    <div class="text-right"> <button class="btn small save-it-btn">Sačuvaj</button> </div>
					</div>
	            </form>
			</div>

		</div>
	</div>
	<br>
</section>