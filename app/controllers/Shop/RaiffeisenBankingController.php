 <?php

class RaiffeisenBankingController extends BaseController{

    public function raiffeisen(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);
        

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where(array('web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,'web_nacin_placanja_id' => 3))->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url());
        }

        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return Redirect::to(Options::base_url());
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return Redirect::to(Options::base_url());
        }

        $orgOid = $web_b2c_narudzbina_id;

        $orgAmount = DB::select("SELECT round(SUM(jm_cena*kolicina), 2) AS suma from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id=".$orgOid)[0]->suma;
        if($web_b2c_narudzbina->web_nacin_isporuke_id != 2) {
            $orgAmount += Cart::troskovi($web_b2c_narudzbina_id);
        }
        $orgAmount = number_format($orgAmount,2,",","");



        $amount = str_replace("|", "\\|", str_replace("\\", "\\\\", $orgAmount));



        $MerchantID = Options::gnrl_options(3023,'str_data');
        $TerminalID = Options::gnrl_options(3024,'str_data');
        $PurchaseTime = date('ymdHis');
        $OrderID = Order::broj_dokumenta($orgOid);
        $CurrencyID = '941';
        $TotalAmount = round($amount*100);





        $formated_string = "$MerchantID;$TerminalID;$PurchaseTime;$OrderID;$CurrencyID;$TotalAmount;;";

        openssl_sign($formated_string, $potpis, file_get_contents('files/raiffeisen/215490533257FFC.pem'));
        $potpis = base64_encode($potpis);

        $data = array(
                'MerchantID' => $MerchantID,
                'TerminalID' => $TerminalID,
                'PurchaseTime' => $PurchaseTime,
                'OrderID' => $OrderID,
                'CurrencyID' => $CurrencyID,
                'TotalAmount' => $TotalAmount,
                'Signature' => $potpis,
                'web_kupac'=>$web_kupac,
                'preduzece'=>$preduzece,
                'title' => 'Raiffeisen',
                'description' => 'raiffeisen',
                'keywords' => 'raiffeisen',
                'strana' => 'raiffeisen',
                "org_strana"=>'raiffeisen'
            );


            //All::dd($data);
        return View::make('shop/themes/'.Support::theme_path().'pages/raiffeisen',$data);
    }

    public function raiffeisen_response(){
        $request = Request::all();


        if(!isset($request) || is_null($request) || empty($request)) {
            return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Odustali ste od kredita Raiffeisen banke').'.');
        }


        if(empty($request['ApprovalCode']) && $request['TranCode'] != '000') {

            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['OrderID'])->first();

            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;

            $bank_response = json_encode(array(
                'realized' => 0,
                'payment_id' => isset($request['XID']) ? $request['XID'] : '',
                'oid' => isset($request['OrderID']) ? $request['OrderID'] : '',
                'trans_id' => isset($request['XID']) ? $request['XID'] : '',                
                'track_id' => '',
                'post_date' => date('Y-m-d H:i:s'),
                'result_code' => '',
                'auth_code' => isset($request['ApprovalCode']) ? $request['ApprovalCode'] : '',
                'response' => 'NEUSPEŠNO',
                'md_status' => '',
                'TranCode' => isset($request['TranCode']) ? $request['TranCode'] : '',
                'Currency' => isset($request['Currency']) ? $request['Currency'] : '',
                'Signature' => isset($request['Signature']) ? $request['Signature'] : '',
                'PurchaseTime' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'TotalAmount' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'ProxyPan' => isset($request['ProxyPan']) ? $request['ProxyPan'] : '',
                'Rrn' => isset($request['Rrn']) ? $request['Rrn'] : '',
                'locale' => isset($request['locale']) ? $request['locale'] : '',
                'Email' => isset($request['Email']) ? $request['Email'] : ''
                ));

            $data = array('result_code'=> $bank_response);
            $data['poslednja_izmena']=date('Y-m-d H:i:s');

            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);
        }else{
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['OrderID'])->first();
            if(is_null($web_b2c_narudzbina)){
                return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
            }           
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;
         $bank_response = json_encode(array(
                'realized' => 1,
                'payment_id' => isset($request['XID']) ? $request['XID'] : '',
                'oid' => isset($request['OrderID']) ? $request['OrderID'] : '',
                'trans_id' => isset($request['XID']) ? $request['XID'] : '',                
                'track_id' => '',
                'post_date' => date('Y-m-d H:i:s'),
                'result_code' => '',
                'auth_code' => isset($request['ApprovalCode']) ? $request['ApprovalCode'] : '',
                'response' => 'USPEŠNO',
                'md_status' => '',
                'TranCode' => isset($request['TranCode']) ? $request['TranCode'] : '',
                'Currency' => isset($request['Currency']) ? $request['Currency'] : '',
                'Signature' => isset($request['Signature']) ? $request['Signature'] : '',
                'PurchaseTime' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'TotalAmount' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'ProxyPan' => isset($request['ProxyPan']) ? $request['ProxyPan'] : '',
                'Rrn' => isset($request['Rrn']) ? $request['Rrn'] : '',
                'locale' => isset($request['locale']) ? $request['locale'] : '',
                'Email' => isset($request['Email']) ? $request['Email'] : ''
                ));

                $data = array('result_code'=> $bank_response);


                $data['stornirano'] = 0;
                $data['poslednja_izmena']=date('Y-m-d H:i:s');

                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);

                return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);

        }
        return Redirect::to(Options::base_url().Url_mod::slug_trans('raiffeisen-odgovor-get').'/'.$web_b2c_narudzbina_id);
    }

    public function raiffeisen_notify(){
        $request = Request::all();


        if(!isset($request) || is_null($request) || empty($request)) {
        // return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Odustali ste od kredita Raiffeisen banke').'.');
        echo "MerchantID=\n";
        echo "TerminalID=\n";
        echo "OrderID=\n";
        echo "Delay =\n";
        echo "Currency=\n";
        echo "TotalAmount=\n";
        echo "XID=\n";
        echo "PurchaseTime=\n";
        echo "Response.action= reverse \n";
        echo "Response.reason= something goes wrong \n";
        echo "Response.forwardUrl=".Options::base_url().Url_mod::slug_trans('raiffeisen-failed')."\n";
        die;
        }

        $MerchantID = $request['MerchantID'];
        $TerminalID = $request['TerminalID'];
        $OrderID = $request['OrderID'];
        $Delay = $request['Delay'];
        $PurchaseTime = $request['PurchaseTime'];
        $TotalAmount = $request['TotalAmount'];
        $AltTotalAmount = $request["AltTotalAmount"];
        $CurrencyID = $request['Currency'];
        $XID = $request['XID'];
        $SD = $request['SD'];
        $TranCode = $request['TranCode'];
        $ApprovalCode = $request['ApprovalCode'];
        $signature = $request["Signature"];

        $signature = base64_decode($signature);

        if($AltTotalAmount){
            $data = $MerchantID . ";" . $TerminalID . ";" . $PurchaseTime . ";" . $OrderID . "," . $Delay . ";" . $XID . ";" . $CurrencyID . "," . $AltCurrencyID . ";" . $TotalAmount . "," . $AltTotalAmount . ";" . $SD . ";" . $TranCode . ";" . $ApprovalCode.";";
        }else{
            $data = $MerchantID . ";" . $TerminalID . ";" . $PurchaseTime . ";" . $OrderID . "," . $Delay . ";" . $XID . ";" . $CurrencyID . ";" . $TotalAmount . ";" . $SD . ";" . $TranCode . ";" . $ApprovalCode.";";
        }

        $crtid = openssl_pkey_get_public(file_get_contents('files/raiffeisen/work-server.CRT'));

        $verify_status = openssl_verify($data, $signature, $crtid);
        openssl_free_key($crtid);

        if($verify_status != 1) {

            echo "MerchantID=".$request['MerchantID']."\n";
            echo "TerminalID=".$request['TerminalID']."\n";
            echo "OrderID=".$request['OrderID']."\n";
            echo "Delay = ".$request['Delay']."\n";
            echo "Currency=".$request['Currency']."\n";
            echo "TotalAmount=".$request['TotalAmount']."\n";
            echo "XID=".$request['XID']."\n";
            echo "PurchaseTime=".$request['PurchaseTime']."\n";

            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['OrderID'])->first();

            if(is_null($web_b2c_narudzbina)){
                // return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
                echo "Response.action= reverse \n";
                echo "Response.reason= something goes wrong \n";
                echo "Response.forwardUrl=".Options::base_url().Url_mod::slug_trans('raiffeisen-failed')."\n";
                die;
            }
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;

            $bank_response = json_encode(array(
                'realized' => 0,
                'payment_id' => isset($request['XID']) ? $request['XID'] : '',
                'oid' => isset($request['OrderID']) ? $request['OrderID'] : '',
                'trans_id' => isset($request['XID']) ? $request['XID'] : '',                
                'track_id' => '',
                'post_date' => date('Y-m-d H:i:s'),
                'result_code' => '',
                'auth_code' => isset($request['ApprovalCode']) ? $request['ApprovalCode'] : '',
                'response' => 'NEUSPEŠNO',
                'md_status' => '',
                'TranCode' => isset($request['TranCode']) ? $request['TranCode'] : '',
                'Currency' => isset($request['Currency']) ? $request['Currency'] : '',
                'Signature' => isset($request['Signature']) ? $request['Signature'] : '',
                'PurchaseTime' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'TotalAmount' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'ProxyPan' => isset($request['ProxyPan']) ? $request['ProxyPan'] : '',
                'Rrn' => isset($request['Rrn']) ? $request['Rrn'] : '',
                'locale' => isset($request['locale']) ? $request['locale'] : '',
                'Email' => isset($request['Email']) ? $request['Email'] : ''
                ));

            $data = array('result_code'=> $bank_response);
            $data['poslednja_izmena']=date('Y-m-d H:i:s');

            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);

                echo "Response.action= approve \n";
                echo "Response.reason= ok \n";
                echo "Response.forwardUrl=".Options::base_url().Url_mod::slug_trans('raiffeisen-odgovor').'/'.$web_b2c_narudzbina_id."\n";
                die;
        }else{
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$request['OrderID'])->first();

            echo "MerchantID=".$request['MerchantID']."\n";
            echo "TerminalID=".$request['TerminalID']."\n";
            echo "OrderID=".$request['OrderID']."\n";
            echo "Delay = ".$request['Delay']."\n";
            echo "Currency=".$request['Currency']."\n";
            echo "TotalAmount=".$request['TotalAmount']."\n";
            echo "XID=".$request['XID']."\n";
            echo "PurchaseTime=".$request['PurchaseTime']."\n";

            if(is_null($web_b2c_narudzbina)){
                // return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
                echo "Response.action= reverse \n";
                echo "Response.reason= something goes wrong \n";
                echo "Response.forwardUrl=".Options::base_url().Url_mod::slug_trans('raiffeisen-failed')."\n";
                die;
            }           
            $web_b2c_narudzbina_id = $web_b2c_narudzbina->web_b2c_narudzbina_id;
         $bank_response = json_encode(array(
                'realized' => 1,
                'payment_id' => isset($request['XID']) ? $request['XID'] : '',
                'oid' => isset($request['OrderID']) ? $request['OrderID'] : '',
                'trans_id' => isset($request['XID']) ? $request['XID'] : '',                
                'track_id' => '',
                'post_date' => date('Y-m-d H:i:s'),
                'result_code' => '',
                'auth_code' => isset($request['ApprovalCode']) ? $request['ApprovalCode'] : '',
                'response' => 'USPEŠNO',
                'md_status' => '',
                'TranCode' => isset($request['TranCode']) ? $request['TranCode'] : '',
                'Currency' => isset($request['Currency']) ? $request['Currency'] : '',
                'Signature' => isset($request['Signature']) ? $request['Signature'] : '',
                'PurchaseTime' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'TotalAmount' => isset($request['PurchaseTime']) ? $request['PurchaseTime'] : '',
                'ProxyPan' => isset($request['ProxyPan']) ? $request['ProxyPan'] : '',
                'Rrn' => isset($request['Rrn']) ? $request['Rrn'] : '',
                'locale' => isset($request['locale']) ? $request['locale'] : '',
                'Email' => isset($request['Email']) ? $request['Email'] : ''
                ));

                $data = array('result_code'=> $bank_response);


                $data['stornirano'] = 0;
                $data['poslednja_izmena']=date('Y-m-d H:i:s');

                DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);

                echo "Response.action= approve \n";
                echo "Response.reason= ok \n";
                echo "Response.forwardUrl=".Options::base_url().Url_mod::slug_trans('raiffeisen-narudzbina').'/'.$web_b2c_narudzbina_id."\n";
                die;

                // return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);

        }
        // return Redirect::to(Options::base_url().Url_mod::slug_trans('raiffeisen-odgovor').'/'.$web_b2c_narudzbina_id);
    }

    public function raiffeisen_failed() {
         return Redirect::to(Options::base_url().Url_mod::slug_trans('korpa'))->with('failure-message',Language::trans('Došlo je do greške. Račun platne kartice nije zadužen').'.');
    }

    public function raiffeisen_narudzbina() {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

       return Redirect::to(Options::base_url().Url_mod::slug_trans('narudzbina').'/'.$web_b2c_narudzbina_id);
    }

    public function raiffeisen_failure(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('Raiffeisen greska')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

    public function raiffeisen_failure_get(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('Raiffeisen greska')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }



}