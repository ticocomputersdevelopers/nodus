<?php

use Illuminate\Support\Facades\Response;
use Service\Mailer;

class RmaLoginController extends Controller {

    function login(){
        if(AdminOptions::gnrl_options(3040) == 0){
            return Redirect::to(Options::base_url());
        }
        if(RmaOptions::user()){
            return Redirect::to(RmaOptions::base_url().'rma');
        }

        $data=array(
            'strana'=>'login',
            "title"=>'Prijava',
            "description"=>'Prijava',
            "keywords"=>'prijava',

        );
        return View::make('rma.pages.login',$data);
    }

    function loginStore(){
        $input = Input::all();

        $validator = Validator::make($input,array(
                'username' => 'required|between:3,50',
                'password' => 'required|between:3,50|exists:partner,password,login,'.$input['username'].''
            ),
            array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Uneli ste ne dozvoljene karaktere!',
                'between' => 'Vaš sadzaj polja je prekratak ili predugačak!',
                'exists' => 'Uneli ste pogrešno korisničko ime ili lozinku!',
            )
        );
        if ($validator->fails()) {
            return Redirect::to(RmaOptions::base_url().'rma/login')->withInput()->withErrors($validator);

        }else{

            $partner = B2bPartner::where('login',$input['username'])->where('password',$input['password'])->first();
            Session::put('rma_user_'.Options::server(),$partner->partner_id);
            Session::put('rma_user_kind_'.Options::server(),'servis');

            return Redirect::to(RmaOptions::base_url().'rma');
        }
    }

    public function logout(){
        Session::forget('rma_user_'.Options::server());
        Session::forget('rma_user_kind_'.Options::server());
        return Redirect::to(RmaOptions::base_url().'rma/login');
    }

}