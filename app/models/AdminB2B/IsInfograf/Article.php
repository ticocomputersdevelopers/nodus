<?php
namespace IsInfograf;
use DB;
use AdminB2BOptions;

class Article {

	public static function table_body($articles,$mappedGroups,$mappedManufacturers){
		$result_arr = array();
		$course = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->first();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			$id_is = $article->IDArtikal;

			$roba_id++;
			$sifra_k++;
			$sifra_is = $article->sifra;
			$naziv = pg_escape_string(substr(Support::convert($article->artikal),0,300));
			$tarifna_grupa_id = 0;
			$jedinica_mere_id = Support::getJedinicaMereId($article->garancija);
			$proizvodjac_id = isset($mappedManufacturers[strval($article->proizvodjac)]) ? $mappedManufacturers[strval($article->proizvodjac)] : -1;
			$grupa_pr_id = isset($mappedGroups[strval($article->grupa)]) ? $mappedGroups[strval($article->grupa)] : -1;
			$akcija = isset($article->na_akciji) && $article->na_akciji == 1 ? 1 : 0;
			$naziv_dopunski = pg_escape_string(substr(Support::convert($article->opis),0,200));
			$web_opis = pg_escape_string(nl2br(htmlspecialchars(Support::convert($article->opis_long))));
			$flag_cenovnik = (!is_null($article->mag) && !is_null($article->kol) && is_numeric($article->kol) && intval($article->kol) > 0) ? 1 : 0;

			$racunska_cena_nc = 0;
			$racunska_cena_end = 0;
			$web_cena = 0;
			if(isset($article->cena) && is_numeric($article->cena) && $article->cena > 0){
				$racunska_cena_nc = floatval($article->cena) * $course->ziralni;
				$racunska_cena_end = $racunska_cena_nc;
				$web_cena = $racunska_cena_nc * 1.2;
			}
			$mpcena = $web_cena;
			$akcijska_cena = 0;

			$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."','".$naziv_dopunski."',NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".strval($flag_cenovnik).",0,NULL,1,".strval($racunska_cena_nc).",".strval($racunska_cena_end).",".strval($racunska_cena_end).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',".strval($flag_cenovnik).",NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,".strval($akcija).",0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,".strval($akcijska_cena).",0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,(NULL)::integer)";

		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}