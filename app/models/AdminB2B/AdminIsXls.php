<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsXls\ReadFile;
use IsXls\Article;
use IsXls\Group;
use IsXls\Lager;
use IsXls\Osobine;
use IsXls\Images;
use IsXls\Support;

class AdminIsXls {


    public static function xls_execute(){
        $result = ReadFile::articles();
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }

    public static function xlsx_execute(){
        $result = ReadFile::articles(true);
        if($result->success){        
            self::execute($result->articles);
        }
        return $result->success;
    }
    
    public static function execute($articles){
        
        // // //groups        
        $groups=ReadFile::groups();
        //All::dd($groups);
        $resultGroup = Group::table_body($groups);
        Group::query_insert_update($resultGroup->body,array('grupa'));
        Support::updateGroupsParent();       

        //articles
        //$articles=ReadFile::articles();
        $resultArticle = Article::table_body($articles);
        if($resultArticle->body!=''){
            Article::query_insert_update($resultArticle->body,array('grupa_pr_id'));
            // Article::query_update_unexists($resultArticle->body);
        }
        //osobine
        $resultOsobine = Osobine::table_body($articles);
        if($resultOsobine->osobina_kombinacija !=''){
            Osobine::query_insert_update($resultOsobine);
            Osobine::update_osobina_kombinacija_vrednost($articles);
        }
        // lager
        $mappedArticles = Support::getMappedArticles();
        $resultStock = lager::table_body($articles,$mappedArticles);
        lager::query_insert_update($resultStock->body);

    }

}