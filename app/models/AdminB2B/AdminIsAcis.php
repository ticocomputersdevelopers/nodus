<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsAcis\APIData;
use IsAcis\Article;
use IsAcis\ArticleGroup;
use IsAcis\Group;
use IsAcis\Stock;
use IsAcis\Partner;
use IsAcis\Price;
use IsAcis\PartnerCard;
use IsAcis\PartnerRabat;
use IsAcis\Support;



class AdminIsAcis {

    public static function execute(){

        // try {
           
            $acis_data = AdminB2BOptions::info_sys('acis');
            
            $username = $acis_data->username;
            $password = $acis_data->password;
            $portal = $acis_data->portal;
            $api_url = $acis_data->api_url;           
            
            $mappedArticles = Support::getMappedArticles();
            //articles
            $articles = APIData::artikli($username,$password,$portal,$api_url);
            $groups = Support::filteredGroups($articles);            
            Support::saveGroups($groups);

            $mapped_groups = Support::getMappedGroups();
            $resultArticle = Article::table_body($articles,$mapped_groups);
            Article::query_insert_update($resultArticle->body,array('sifra_d'));
            Article::query_update_unexists($resultArticle->body);

            // //articles_groups
            $articles_groups = APIData::artikli($username,$password,$portal,$api_url);
            //Support::updateAricleMainGroups($articles_groups->main_articles_groups,$mappedArticles,$mappedGroups);
            $resultArticleGroup = ArticleGroup::table_body($articles_groups,$mappedArticles,$mapped_groups);
            ArticleGroup::query_insert_delete($resultArticleGroup->body);
         
            //price
            $prices = APIData::lager($username,$password,$portal,$api_url);
            $resultPrice = Price::table_body($prices,$mappedArticles);
            Price::query_update($resultPrice->body);
            
            //lager
            $stock = APIData::lager($username,$password,$portal,$api_url);
            $resultLager = Stock::table_body($stock,$mappedArticles,1);
            Stock::query_insert_update($resultLager->body);

            //partneri
            $partners = APIData::partners($username,$password,$portal,$api_url);
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','rabat','mesto','telefon','pib','broj_maticni'));
            die;
            // //magacin
            // // $werhouse = APIData::magacini($token);
            // // foreach ($werhouse as $whr) {
            // //    Support::getOrgjId($whr->warehouse_code);
            // // }
            // // $mappedWerhouse = Support::getMappedOrgj();



            //die;
            // $grupe = APIData::grupe($token);
            // //$internalGroupMapped = Support::internalGroupMapped($grupe);
            // $resultGroup = Group::table_body($grupe);
            // Group::query_insert_update($resultGroup->body,array('grupa','parrent_grupa_pr_id'));
            // Group::query_update_unexists($resultGroup->body);
            // die;
            //partner
            // $partners = APIData::partners();
            // $resultPartner = Partner::table_body($partners);
            // Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','rabat','mesto','telefon','pib','broj_maticni'));
            // // Partner::query_delete_unexists($resultPartner->body);
            // $mappedPartners = Support::getMappedPartners();

            // //partner card
            // $partnersCards = APIData::partnersCards();
            // $resultPartnerCard = PartnerCard::table_body($partnersCards,$mappedPartners);
            // PartnerCard::query_insert_update($resultPartnerCard->body);
            // PartnerCard::query_delete_unexists($resultPartnerCard->body);

            // $resultStock = Stock::table_body($articles,$mapped_articles);
            // Stock::query_insert_update($resultStock->body);

            // $partner_rabat = APIData::partnerGroupRabat();
            // $resultPartnerRabat = PartnerRabat::table_body($partner_rabat,$mappedPartners,$mapped_groups);
            // PartnerRabat::query_insert_update($resultPartnerRabat->body);
            // PartnerRabat::query_delete_unexists($resultPartnerRabat->body);

            // // Support::postUpdate();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        
        // }catch (Exception $e){
        //     AdminB2BIS::saveISLog('false');
        //     AdminB2BIS::saveISLogError($e->getMessage());
        //     AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
        //     return (object) array('success'=>false,'message'=>$e->getMessage());
        // }
    }




}