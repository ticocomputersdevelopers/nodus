<?php

class AdminB2BOptions {

	public static function base_url(){
		
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
		
	}

    public static function server(){
        return DB::table('options')->where('options_id',1326)->pluck('str_data');
    }

    public static function options($option_id,$str_data=true){
        if($str_data){
            return DB::table('options')->where('options_id',$option_id)->pluck('str_data');
        }else{
            return DB::table('options')->where('options_id',$option_id)->pluck('int_data');
        }
    }

	public static function checkB2B(){
		if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(1,2))){
			return true;
		}else{
			return false;
		}
	}
    public static function checkB2C(){
        if(in_array(DB::table('web_options')->where('web_options_id',130)->pluck('int_data'),array(0,1))){
            return true;
        }else{
            return false;
        }
    } 
    public static function vodjenje_lageraB2B(){
                $cn_query=DB::table('web_options')->where('web_options_id',210)->get();
            foreach($cn_query as $row){
                return $row->int_data;
            }
    }
    public static function check_admin($sifra=null){
        $imenik_id = Session::get('b2c_admin'.self::server());
        if($imenik_id == 0){
            return true;
        }
        if($sifra == null){
            return false;
        }       
        $ac_group_id = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
        $ac_module_id = DB::table('ac_module')->where('sifra', $sifra)->pluck('ac_module_id');
        $count = DB::table('ac_group_module')->where(array('ac_group_id'=>intval($ac_group_id),'alow'=>1))->where('ac_module_id', $ac_module_id)->count();

        if($count > 0){
            return true;
        }else{
            return false;
        }
    }
    public static function get_admin(){
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.self::server()))->first();
        return $user->ime.' '.$user->prezime;
    }
    public static function checkB2BNarudzbine(){
		if(DB::table('web_options')->where('web_options_id',135)->pluck('int_data')==1){
			return true;
		}else{
			return false;
		}
	}
	public static function search_conver($str){
		 $string_new=strtolower($str); 
		$urlKey = strtr($string_new, array("&frasl;"=>"/"," "=>"%"));
	
		return $urlKey;
	}
	public static function text_convert($string){
		$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;",'"'=>""));
    	return $urlKey;
	}
    public static function limit_liste_robe(){
        $limit =  DB::table('options')->where('options_id',1329)->pluck('int_data');
        if($limit > 400){
            $limit = 400;
        }
        return $limit;
    }
    
    public static function info_sys($sifra=null){
        $conditions = array('aktivan'=>1,'izabran'=>1);
        if(!is_null($sifra)){
            $conditions['sifra'] = $sifra;
        }
        return DB::table('informacioni_sistem')->where($conditions)->first();
    }

    public static function kurs($valuta_id=null){
        $def_valuta_id = DB::table('valuta')->where('izabran',1)->pluck('valuta_id');
        if(is_null($valuta_id)){
            if(Session::has('b2b_valuta') && is_numeric(Session::get('b2b_valuta'))){
                $valuta_id = Session::get('b2b_valuta');
            }else{
                $valuta_id = $def_valuta_id;
                Session::put('b2b_valuta',$valuta_id);
            }
        }

        if($valuta_id == $def_valuta_id){
            return 1;
        }
        
        $obj = DB::table('kursna_lista')->where('valuta_id',$valuta_id)->orderBy('kursna_lista_id','desc')->first();
        $vrednost = 1;
        if(isset($obj)){
            $izabrani_kurs = self::web_options(205,'str_data');
            if($izabrani_kurs == 'kupovni'){
                $vrednost = $obj->kupovni;
            }elseif($izabrani_kurs == 'srednji'){
                $vrednost = $obj->srednji;
            }elseif($izabrani_kurs == 'ziralni'){
                $vrednost = $obj->ziralni;
            }elseif($izabrani_kurs == 'prodajni'){
                $vrednost = $obj->prodajni;
            }else{
                $vrednost = $obj->web;
            }
        }
        if($vrednost <= 0){
            $vrednost = 1;
        }
        $vrednost = 1/$vrednost;
        
        return $vrednost;
    }

    public static function gnrl_options($options_id,$kind='int_data'){
        return DB::table('options')->where('options_id',$options_id)->pluck($kind);
    }

    public static function web_options($options_id){
        return DB::table('web_options')->where('web_options_id',$options_id)->pluck('int_data');
    }    


}
