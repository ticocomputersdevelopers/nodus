<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;
use All;
use DB;

class CategoriesExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/grupa_pr.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array("(-1,'Nedefinisana',NULL,(NULL)::integer,-1,1,1,1,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL)","(0,'KATEGORIJE',NULL,-1,0,1,1,1,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL)");
		$category_ids = array(-1,0);

		for ($row = 1; $row <= $lastRow; $row++) {
		    $B = $worksheet->getCell('B'.$row)->getValue();
		    $C = $worksheet->getCell('C'.$row)->getValue();
		    $D = $worksheet->getCell('D'.$row)->getValue();

		    if(is_numeric($B) && isset($C) && is_numeric($D)){
		        $result_arr[] = "(".$B.",'".addslashes(AdminB2BIS::encodeTo1250($C))."',NULL,".$D.",".$B.",1,1,1,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL)";
		        $category_ids[] = intval($B);
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"ids"=>$category_ids);
	}


	public static function query_insert_update($table,$table_temp_body,$upd_cols=array(),$is_upd_seq) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='".$table."'"));
		$table_temp = "(VALUES ".$table_temp_body.") ".$table."_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		//update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		foreach($columns as $col){
			if($col!=$table."_id" && $col!="sifra" && $col!="web_b2b_prikazi" && $col!="redni_broj" && $col!="web_b2c_prikazi" && $col!="prikaz"){
		    	$updated_columns[] = "".$col." = ".$table."_temp.".$col."";
			}
		}
		DB::statement("UPDATE ".$table." t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.".$table."_id=".$table."_temp.".$table."_id ");

		//insert
		DB::statement("INSERT INTO ".$table." (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM ".$table." t WHERE t.".$table."_id=".$table."_temp.".$table."_id))");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		if($is_upd_seq){
			DB::statement("SELECT setval('".$table."_".$table."_id_seq', (SELECT MAX(".$table."_id) FROM ".$table."), FALSE)");
		}
	}

}