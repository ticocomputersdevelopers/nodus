<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsTimCode\TimCodeAPI;
use IsTimCode\Group;
use IsTimCode\Manufacturer;
use IsTimCode\Article;
use IsTimCode\Prices;
use IsTimCode\Lager;
use IsTimCode\Partner;
use IsTimCode\Support;


class AdminIsTimCode {

    public static function execute(){
        //http://sinko-plus.selltico.com/auto-import-is/118b3c86b404de0a45bcf89f70377446

        // if(AdminB2BIS::checkSynchronization()){
        //     return (object) array('success'=>false,'message'=>'Sinhronizacija je već pokrenuta!');
        // }
        // AdminB2BIS::synchronization(true);

        try {
            $infoSys = AdminB2BOptions::info_sys('timcode');
            $auth = array(
                'username' => $infoSys->username,
                'password' => $infoSys->password,
                'firmaNaziv' => $infoSys->portal,
                'godina' => date('Y'),
            );
            $magaciniIds = array(strval($infoSys->b2c_magacin));

            $grupe = TimCodeAPI::groups($auth);
            $resultGroup = Group::table_body($grupe);
            Group::query_insert_update($resultGroup->body,array('grupa','web_b2b_prikazi','web_b2c_prikazi','prikaz'));
            // Group::query_update_unexists($resultGroup->body);
            // Support::updateGroupsParent($grupe);

            $proizvodjaci = TimCodeAPI::manufacturers($auth);
            $resultManufacturers = Manufacturer::table_body($proizvodjaci);
            Manufacturer::query_insert_update($resultManufacturers->body,array('sifra','naziv'));
            Manufacturer::query_delete_unexists($resultManufacturers->body);

            // //articles
            $artikli = TimCodeAPI::articles($auth,$magaciniIds);
            $resultArticle = Article::table_body($artikli);
            $id_iss = Article::query_insert_update($resultArticle->body,array('sifra_is','flag_aktivan','flag_prikazi_u_cenovniku','grupa_pr_id','naziv','naziv_web','tarifna_grupa_id','jedinica_mere_id','proizvodjac_id','racunska_cena_nc','mpcena','web_cena','akcija_flag_primeni'));
            Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();


            // //cene
            // $cene = TimCodeAPI::pricelist($auth);
            // $resultPrices = Prices::table_body($cene,$mappedArticles);
            // Prices::query_insert_update($resultPrices->body,array('mpcena'));


            //lager
            $resultLager = Lager::table_body($artikli,$mappedArticles);
            Lager::query_insert_update($resultLager->body);


            // //partner
            // $partners = TimCodeAPI::parneri_svi($auth);
            // $resultPartner = Partner::table_body($partners);
            // Partner::query_insert_update($resultPartner->body);


            Support::entityDecode();

            // AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        }catch (Exception $e){
            // AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLog('false');
            AdminB2BIS::saveISLogError($e->getMessage());
            AdminB2BIS::sendNotification(array(9,12,15,18),15,5);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}