<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;

class Kupindo {

	public static function execute($export_id,$kind,$config=array()){

		$grupa_query = "";
		if(isset($config['grupa_pr_ids'])){
			$group_ids= array();
			foreach($config['grupa_pr_ids'] as $gr_id){
				AdminCommon::allGroups($group_ids,$gr_id);
			}
			$grupa_query = " AND grupa_pr_id IN (".implode(",",$group_ids).")";
		}

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, opis, garancija, web_opis, roba.grupa_pr_id, garancija, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0".$grupa_query);

		if($kind=='xml'){
			self::xml_exe($export_products,$export_id);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products,$export_id){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("Prodavnica");
		$xml->appendChild($root);
		$predmetiLista = $xml->createElement("PredmetiLista");
		$predmetiLista->setAttribute("type", "array");

		$export_grupe = Support::export_grupe($export_id);

		foreach($products as $article){
			if(isset($export_grupe[$article->grupa_pr_id])){
				$predmet   = $xml->createElement("Predmet");

			    Support::xml_node($xml,"Aktivan",1,$predmet);
			    Support::xml_node($xml,"IDPodKategorija",$export_grupe[$article->grupa_pr_id]->grupa_id,$predmet);
			    Support::xml_node_cdata($xml,"Naziv",$article->naziv_web,$predmet);
			    Support::xml_node_cdata($xml,"Opis",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$predmet);
			    Support::xml_node($xml,"Cena",intval($article->web_cena),$predmet);
			    Support::xml_node($xml,"Kolicina",$article->kolicina,$predmet);
			
				$images = $xml->createElement("Images");
				$images->setAttribute("type", "array");
			    foreach (Support::slike($article->roba_id) as $image ) {
			    	
			    	Support::xml_node($xml,"ImageURL",AdminOptions::base_url().$image,$images);
			    }
			    $predmet->appendChild($images);		    
			  
				
				Support::xml_node($xml,"IDInterni",$article->roba_id,$predmet);
				Support::xml_node($xml,"GTIN",'',$predmet);
				Support::xml_node($xml,"Garancija",($article->garancija != 0 ? 1 : 0),$predmet);
				Support::xml_node_cdata($xml,"DuzinaGarancije",$article->garancija,$predmet);
				Support::xml_node($xml,"Stanje",1,$predmet);

				$nacinslanja = $xml->createElement("NacinSlanja");
				$nacinslanja->setAttribute("type", "array");
			    foreach (array('') as $slanje ) {
			    	
			    	Support::xml_node($xml,"Slanje",4,$nacinslanja);
			    }
			    $predmet->appendChild($nacinslanja);	

			    $nacinpacanja = $xml->createElement("NacinPlacanja");
				$nacinpacanja->setAttribute("type", "array");
			    foreach (array('') as $placanje ) {
			    	
			    	Support::xml_node($xml,"Placanje",4,$nacinpacanja);
			    }
			    $predmet->appendChild($nacinpacanja);

				
				Support::xml_node($xml,"BesplatnaDostava",0,$predmet);
				Support::xml_node($xml,"YoutubeURL",'',$predmet);

				$predmetiLista->appendChild($predmet);
			}
		}

		$root->appendChild($predmetiLista);
		
		$xml->formatOutput = true;
		$store_path = 'files/exporti/kupindo/artikli.xml';
		$xml->save($store_path) or die("Error");

		header('Content-type: text/xml');
		// header('Content-Disposition: attachment; filename="artikli.xml"');
		echo file_get_contents($store_path); die;
	}


}