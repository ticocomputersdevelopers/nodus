<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;
use Groups;

class Nodus {

	public static function execute($export_id,$kind,$short=false){

		$grupa_pr_ids=array(14,12,13,40,31,38,149,49,39,804,398);
		
		$export_products =DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, (select grupa from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = roba.grupa_pr_id)) as nadgrupa, (select grupa from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = roba.grupa_pr_id))) as nadgrupa_nadgrupe,model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND kolicina>0 and orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, akcijska_cena, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0 AND roba_id IN ( select distinct t.roba_id from (
			select
			(select grupa_pr_id from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = 
			(select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1) limit 1)) as nadgrupa_nadgrupe_pr_id,
			(select grupa_pr_id from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1)) nadgrupa_pr_id,
			(select grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id) as grupa_id,
			roba_id from roba r )  t where t.nadgrupa_nadgrupe_pr_id in (".implode(",",$grupa_pr_ids).") or t.nadgrupa_pr_id in (".implode(",",$grupa_pr_ids).") or t.grupa_id in (".implode(",",$grupa_pr_ids).")) ");


		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("products");
		$xml->appendChild($root);

		foreach($products as $article){

			$product   = $xml->createElement("product");

		    Support::xml_node($xml,"sku",$article->roba_id,$product);
		   	Support::xml_node($xml,"category",$article->nadgrupa_nadgrupe.'/'.$article->nadgrupa.'/'.$article->grupa,$product);
		    Support::xml_node_cdata($xml,"product_name",$article->naziv_web,$product);
		    Support::xml_node_cdata($xml,"product_brand",$article->proizvodjac,$product);		    
			Support::xml_node_cdata($xml,"product_description",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);		    
			Support::xml_node($xml,"product_price",$article->web_cena,$product);
		    Support::xml_node($xml,"product_price_discount",($article->akcija_flag_primeni==1?$article->akcijska_cena:0.00),$product);
		    Support::xml_node($xml,"product_stock",$article->kolicina,$product);
		   	$images = $xml->createElement("product_photos");
			foreach(DB::table('web_slika')->where(array('roba_id'=>$article->roba_id, 'flag_prikazi'=>1))->get() as $slika){
				Support::xml_node($xml,"product_photo", AdminOptions::base_url().$slika->putanja,$images);
			}
			$product->appendChild($images);
			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/povoljniracunari/povoljniracunari.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}
	public static function slike($roba_id){
        return array_map('current',DB::table('web_slika')->select('putanja')->where('akcija',0)->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get());	
	}
}