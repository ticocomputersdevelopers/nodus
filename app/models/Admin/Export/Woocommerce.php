<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use DOMDocument;
use AdminOsobine;

class Woocommerce {

	public static function execute($export_id,$kind,$config){

		$export_products = DB::select("SELECT roba_id, sku, web_cena, mpcena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, grupa_pr_id, keywords, pregledan_puta, proizvodjac_id, barkod, akcija_flag_primeni, flag_prikazi_u_cenovniku, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			self::xml_exe($export_products,$config);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products,$config){

		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("products");
		$xml->appendChild($root);
		foreach($products as $article){

			$product   = $xml->createElement("product");

		    Support::xml_node($xml,"SKU",$article->roba_id,$product);
		    if(isset($config['brand'])){ Support::xml_node($xml,"Brand_Name",$article->proizvodjac,$product); }
		    if(isset($config['name'])){ Support::xml_node($xml,"Product_Name",$article->naziv_web,$product); }
			if(isset($config['description'])){ Support::xml_node($xml,"Long_Description",(isset($article->web_opis)?"<p>".$article->web_opis."</p>":"").self::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product); }
		    if(isset($config['short_description'])){ Support::xml_node($xml,"Short_Description",(isset($article->web_opis)?"<p>".$article->web_opis."</p>":$article->naziv_web),$product); }
		    if(isset($config['category'])){
		    	$grupe = array();
		    	self::parent_categories($grupe,$article->grupa_pr_id);
		    	krsort($grupe);
		    	$sve_grupe = array();
		    	foreach($grupe as $val) {
		    		$sve_grupe[] = $val;
		    	}
		    	if(!isset($sve_grupe[1])){
			    	Support::xml_node($xml,"Category",$sve_grupe[0],$product);
			    	Support::xml_node($xml,"SubCategory",'',$product);
			    	Support::xml_node($xml,"Product_Group",'',$product);
			    }else{
			    	if(isset($sve_grupe[2])){
				    	Support::xml_node($xml,"Category",$sve_grupe[2],$product);
				    	Support::xml_node($xml,"SubCategory",$sve_grupe[1],$product);
				    }else{
				    	Support::xml_node($xml,"Category",$sve_grupe[1],$product);
				    	Support::xml_node($xml,"SubCategory",'',$product);
				    }
			    	Support::xml_node($xml,"Product_Group",$sve_grupe[0],$product);
			    }
		    }
		    if(isset($config['small_image'])){ Support::xml_node($xml,"Thumb_URL",self::slike($article->roba_id),$product); }
		    if(isset($config['image'])){ Support::xml_node($xml,"Image_URL",self::slike($article->roba_id),$product); }
		    if(isset($config['link'])){ Support::xml_node($xml,"Buy_Link",isset($article->flag_prikazi_u_cenovniku)?Support::product_link($article->roba_id):'',$product); }
		    if(isset($config['keywords'])){ Support::xml_node($xml,"Keywords",self::kwards($article->keywords,$article->naziv_web),$product); }
		    if(isset($config['tags'])){ Support::xml_node($xml,"Tags",'',$product); }
			if(isset($config['reviews'])){ Support::xml_node($xml,"Reviews",($article->pregledan_puta!=0?$article->pregledan_puta:''),$product); }
			if(isset($config['weight'])){ Support::xml_node($xml,"Weight",'',$product); }
		    if(isset($config['mp_price'])){Support::xml_node($xml,"Retail_Price",$article->mpcena>0?$article->mpcena:$article->web_cena,$product); }
		    if(isset($config['price'])){ Support::xml_node($xml,"Sale_Price",$article->web_cena,$product); }
		    if(isset($config['brand_link'])){ Support::xml_node($xml,"Brand_Page_Link",self::brand_link($article->proizvodjac_id),$product); }
		    if(isset($config['brand_image'])){ Support::xml_node($xml,"Brand_Logo_Image",self::brand_image($article->proizvodjac_id),$product); }
		    if(isset($config['variants'])){
    			$xml_atributes = $xml->createElement("Extended_Xml_Attributes");
    			if(AdminOsobine::check_osobine($article->roba_id)){    				
    				$variants = $xml->createElement("variants");
	    			foreach(self::combinations(self::variant_arrays($article->roba_id)) as $key => $combination){    			
	    				$variant = $xml->createElement("variant");
	    				Support::xml_node($xml,"sku",$article->roba_id.'-'.($key+1),$variant);
	    				Support::xml_node($xml,"upc",isset($article->barkod)?$article->barkod:'',$variant);
	    				foreach($combination as $comb){ 
					    	Support::xml_node($xml,$comb->naziv,$comb->vrednost,$variant);
					    }
					    if(isset($config['mp_price'])){Support::xml_node($xml,"Retail_Price",$article->mpcena>0?$article->mpcena:$article->web_cena,$variant); }
					    if(isset($config['price'])){ Support::xml_node($xml,"Sale_Price",$article->web_cena,$variant); }
					    Support::xml_node($xml,"action_url",isset($article->akcija_flag_primeni)&&$article->flag_prikazi_u_cenovniku==1?Support::product_link($article->roba_id):'',$variant);
					    $variants->appendChild($variant);
	    			}
	    			$xml_atributes->appendChild($variants);
    			}
			    $product->appendChild($xml_atributes);
		    }
			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/woocommerce/woocommerce-export.xml';
		$xml->save($store_path) or die("Error");

		header('Content-type: text/xml');
		// header('Content-Disposition: attachment; filename="artikli.xml"');
		echo file_get_contents($store_path); die;	
	}

	public static function slike($roba_id){
        $slike = DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get();
	    $sl_str = '';
	    foreach($slike as $sl){
	    	$sl_str .= AdminOptions::base_url().$sl->putanja.'|';
	    }
	    if($sl_str == ''){
	    	return $sl_str;
	    }else{
	    	return substr($sl_str,0,-1);
	    }
	}

	public static function parent_categories(&$array,$grupa_pr_id){
		$row = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
		array_push($array,$row->grupa);
		if($row->parrent_grupa_pr_id > 0){
			self::parent_categories($array,$row->parrent_grupa_pr_id);
		}
	}
	public static function kwards($keywords,$naziv_web){
		if(isset($keywords)){
			return $keywords;
		}
		else{
			$naziv_arr = explode(' ',str_replace(array(','),array(''),$naziv_web));
			return strtolower(implode(', ',array_diff($naziv_arr,array(' '))));
		}
	}

	public static function brand_link($proizvodjac_id){
		$row = DB::table('proizvodjac')->where(array('brend_prikazi'=>1,'proizvodjac_id'=>$proizvodjac_id))->first();
		$link = '';
		if(isset($row->naziv)){
			$link = AdminOptions::base_url() .'proizvodjac/'. AdminOptions::slug_trans($row->naziv);
		}
		return $link;
	}

	public static function brand_image($proizvodjac_id){
		$row = DB::table('proizvodjac')->where(array('brend_prikazi'=>1,'proizvodjac_id'=>$proizvodjac_id))->first();
		$image = '';
		if(isset($row)){		
			if($row->slika != null OR $row->slika != ''){
				$image = AdminOptions::base_url().$row->slika;
			}
		}
		return $image;
	}

	public static function characteristics($roba_id,$web_flag_karakteristike,$karakteristike){
		if($web_flag_karakteristike == 0){
			return '<p>'.$karakteristike.'</p>';
		}
		elseif($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$generisane_karakteristike = '<table>';
			foreach($generisane as $row){
				$generisane_karakteristike .= '<tr><td>'.$row->naziv.'</td><td>'.$row->vrednost.'</td></tr>';
			}
			return $generisane_karakteristike.'</table>';
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$dobavljac_karakteristike = '<table>';
			foreach($dobavljac as $row){
				$dobavljac_karakteristike .= '<tr><td>'.$row->karakteristika_naziv.'</td><td>'.$row->karakteristika_vrednost.'</td></tr>';
			}
			return $dobavljac_karakteristike.'</table>';
		}		
	}

	public static function variant_arrays($roba_id){
		$rows = DB::table('osobina_roba')->select('osobina_naziv_id')->distinct()->where(array('aktivna'=>1,'roba_id'=>$roba_id))->get();
		$arrays = array();
		foreach($rows as $k => $row){
			$arrays[$k] = array();
			$osobine = DB::table('osobina_roba')->where(array('roba_id'=>$roba_id,'aktivna'=>1,'osobina_naziv_id'=>$row->osobina_naziv_id))->orderBy('rbr','asc')->get();
			foreach($osobine as $osobina){
				array_push($arrays[$k],(object) array('naziv'=>AdminOsobine::findProperty($row->osobina_naziv_id,'naziv'),'vrednost'=>AdminOsobine::findPropertyValue($osobina->osobina_vrednost_id,'vrednost')));
			}
		}
		return $arrays;
	}

	public static function combinations($arrays, $i = 0) {
	    if (!isset($arrays[$i])) {
	        return array();
	    }
	    if ($i == count($arrays) - 1) {
	        return $arrays[$i];
	    }
	    $tmp = self::combinations($arrays, $i + 1);
	    $result = array();
	    foreach ($arrays[$i] as $v) {
	        foreach ($tmp as $t) {
	            $result[] = is_array($t) ? 
	                array_merge(array($v), $t) :
	                array($v, $t);
	        }
	    }
	    return $result;
	}

}