<?php
namespace DirectImport;

use DB;
use SimpleXMLElement;


class Support {

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			$new_proizvodjac_id = DB::select("SELECT MAX(proizvodjac_id) + 1 AS max FROM proizvodjac")[0]->max;
			DB::table('proizvodjac')->insert(array(
				'proizvodjac_id' => $new_proizvodjac_id,
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac), FALSE)");
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaId($grupa){
		$gr = DB::table('grupa_pr')->where('grupa',$grupa)->first();

		if(is_null($gr)){
			$new_grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $new_grupa_pr_id,
				'grupa' => $grupa,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $new_grupa_pr_id
				));
			DB::statement("SELECT setval('grupa_pr_grupa_pr_id_seq', (SELECT MAX(grupa_pr_id) FROM grupa_pr), FALSE)");
			$gr = DB::table('grupa_pr')->where('grupa',$grupa)->first();
		}
		return $gr->grupa_pr_id;
	}

	public static function getTarifnaGrupaId($porez){
		if(!is_numeric($porez)){
			return DB::table('tarifna_grupa')->where('default_tarifna_grupa',1)->pluck('tarifna_grupa_id');
		}
		$porez = intval($porez);
		$tg = DB::table('tarifna_grupa')->where('porez',$porez)->first();

		if(is_null($tg)){
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1,
				'sifra' => substr(strval($porez).'00',0,2),
				'naziv' => 'Porez '.strval($porez).'%',
				'porez' => $porez,
				'active' => 1
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$porez)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function htmlCharacteristics($characteristics_groups){
		$html = '<ul class="generated-features-list row">';
		foreach ($characteristics_groups as $key => $characteristics_group) {
			$html .= '<li class="medium-4 columns features-list-title"><span>'.$characteristics_group->name.'</span></li>
		                <li class="medium-8 columns features-list-items">
		                <ul class="row">';
		    foreach ($characteristics_group->characteristics as $key2 => $characteristic) {
		    	$html .= '<li class="medium-6 columns">'.$characteristic->name.': </li><li class="medium-6 columns">'.$characteristic->value.'</li>';
		    }
		    $html .= '</ul></li>';
		}
		$html .= '</ul>';
		return $html;
	}
	public static function encodeTo1250($string){
		return iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
	}
		public static function charset_decode_utf_8($string)
    {
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        return $string;
    }
    public static function filteredGroups($articles){
		$groups = array();
		$mapp = array();
		foreach($articles as $article) {
			
			if(isset($article->group) && $article->group != ''){
				$groups[mb_convert_encoding($article->group,mb_detect_encoding($article->group),"UTF-8")] = mb_convert_encoding($article->Artikal_Grupa_ID,mb_detect_encoding($article->Artikal_Grupa_ID),"UTF-8");

				$mapp[mb_convert_encoding($article->group,mb_detect_encoding($article->group),"UTF-8")] = Support::convert($article->Artikal_Kategorija_Naziv);
			}
			if(isset($article->Artikal_Grupa_ID) && $article->Artikal_Grupa_ID != ''){
				$mapp[mb_convert_encoding($article->Artikal_Grupa_ID,mb_detect_encoding($article->Artikal_Grupa_ID),"UTF-8")] = Support::convert($article->Artikal_Grupa_Naziv);
			}
		}
		return (object) ['tree' => $groups, 'mapp' => $mapp];
		
	}
	public static function saveGroups($groups){
		foreach($groups->tree as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups->tree,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($groups->mapp,$tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('sifra_is')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->code] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->code] = $article->roba_id;
		}
		return $mapped;
	}
	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}


}