<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Westaco {

	public static function execute($dobavljac_id,$extension=null){
		
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/westaco/westaco.xml');
			$products_file = "files/westaco/westaco.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$products = simplexml_load_file($products_file);	
		
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';
				$sifra = (string) $product->ident;
				$barkod = (string) $product->barcode;
				$grupa = (string) $product->category;
				$proizvodjac = (string) $product->brand;
				$naziv = (string) $product->name;
				$opis = (string) $product->description;
				$slika = (string) $product->image;
				$kolicina = (int) $product->stock;
				$cena_nc = (float) $product->vp_price;
				$mpcena = (float) $product->mp_price;
		
				if (is_numeric($cena_nc) && $cena_nc>0) {
				
				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";
				$sPolja .= " barkod,";					$sVrednosti .= " '" . addslashes($barkod) . "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa). "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . Support::encodeTo1250($proizvodjac). "',";
				$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250(ucfirst(mb_strtolower($naziv)))). "',";
				if (isset($opis) && !empty($opis)) {
					$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "1,";
					$sPolja .= " opis,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($opis)). "',";
				}
				if (isset($slika) && !empty($slika)) {
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= "1,";
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$sifra."','".addslashes(Support::encodeTo1250($slika))."',1 )");
				}
				$sPolja .= " pdv,";					$sVrednosti .= "20,";
				$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina. ",";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena_nc . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/westaco/westaco.xml');
			$products_file = "files/westaco/westaco.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
	
		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->ident;
				$kolicina = (int) $product->stock;
				$cena_nc = (float) $product->vp_price;
				$mpcena = (float) $product->mp_price;

				if (is_numeric($cena_nc) && $cena_nc>0) {
			
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= "" .$kolicina. ",";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena_nc . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function lower($string){
		$firstletter = str_split($string)[0];
		$lower = mb_strtolower($string);
		return $firstletter.substr($lower,1);
	}
	public static function download() {
		$username = 'KLIKLAK.B2B';
		$password = 'wsx678';
		//$loginUrl = 'https://www.moglytoys.com/dilerkatalog/login/main_login.php';
		$file='files/westaco/westaco.csv';

        $userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
        $cookieFile = 'cookie.txt';
        $loginFormUrl = 'https://www.merp-b2b.com/westaco/login';
        $loginActionUrl = 'https://www.merp-b2b.com/westaco/LoginServlet';

        $postValues = array(
            'user' => $username,
            'password' => $password,
            'lang' => 'sr'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($curl);
        if(curl_errno($curl)){
            throw new Exception(curl_error($curl));
        }
        // All::dd(curl_getinfo($curl));
//echo $response; die;

        curl_setopt($curl, CURLOPT_URL, 'https://www.merp-b2b.com/westaco/getItemsAllCsv');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
        curl_exec($curl);
        curl_close($curl);

	}
}