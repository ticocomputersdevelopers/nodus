<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Epi {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$username=Support::serviceUsername($dobavljac_id);
			$password=Support::servicePassword($dobavljac_id);

			self::autoDownload($username,$password,'files/epi/epi_xml/epi.xml');
			//Support::autoDownload(Support::autoLink($dobavljac_id),'files/epi/epi_xml/epi.xml');
			$products_file = "files/epi/epi_xml/epi.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$kurs_new=self::get_kurs($username,$password);

			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->ARTIKAL;		
				$grupa=$product->KATEGORIJA;		
				$podgrupa = $product->PODKATEGORIJA;		
				$kolicina=$product->KOLICINA;	
				$sifra=$product->SIFRA;
				$cena = $product->CENA;
				$barkod = $product->barcode;

				$cena= $cena*$kurs_new;

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " barkod,";					$sVrednosti .= " '" . addslashes($barkod) . "',";			
				$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
				$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$cena, 2, '.', ''),1,'',''). ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$username=Support::serviceUsername($dobavljac_id);
			$password=Support::servicePassword($dobavljac_id);

			self::autoDownload($username,$password,'files/epi/epi_xml/epi.xml');
			//Support::autoDownload(Support::autoLink($dobavljac_id),'files/epi/epi_xml/epi.xml');
			$products_file = "files/epi/epi_xml/epi.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$kurs_new=self::get_kurs($username,$password);

			$products = simplexml_load_file($products_file);
				
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->ARTIKAL;		
				$grupa=$product->KATEGORIJA;		
				$podgrupa = $product->PODKATEGORIJA;		
				$kolicina=$product->KOLICINA;	
				$sifra=$product->SIFRA;
				$cena = $product->CENA;

				$cena= $cena*$kurs_new;

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena . ",";
				$sPolja .= "kolicina";					$sVrednosti .= "" .$kolicina. "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	
	}

	public static function autoDownload($username,$password,$file){
		
		$userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
		$cookieFile = 'cookie.txt';
		$loginFormUrl = 'http://www.epicomputers.rs/main.php#/login/signin';
		$loginActionUrl = 'http://www.epicomputers.rs/login.php';
	
		$postValues = array(
		    'user' => $username,
		    'pass' => $password
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
		curl_exec($curl);
		// echo $curl;die;
		if(curl_errno($curl)){
		    throw new Exception(curl_error($curl));
		}
		curl_setopt($curl, CURLOPT_URL, 'http://www.epicomputers.rs/assets/download.php?tip=XML');
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
		curl_exec($curl);
		curl_close($curl);
	}
	public static function get_kurs($username,$password){
		
		$userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
		$cookieFile = 'cookie.txt';
		$loginFormUrl = 'http://www.epicomputers.rs/main.php#/login/signin';
		$loginActionUrl = 'http://www.epicomputers.rs/login.php';
	
		$postValues = array(
		    'user' => $username,
		    'pass' => $password
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $loginActionUrl);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postValues));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_REFERER, $loginFormUrl);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookies/cookies.txt');
    	curl_setopt($curl, CURLOPT_COOKIEFILE, 'cookies/cookies.txt');
		curl_exec($curl);
		$response = curl_exec($curl);

		if(curl_errno($curl)){
		    throw new Exception(curl_error($curl));
		}
	    $result = 1;
	    preg_match('/<input class=rdio type="radio" id="krsa" name="kurs" value="(.*?)" >/i',$response,$matches);
	    if(count($matches) > 1){
	        $result = floatval($matches[1]);	        
	    } 
	    return $result;
	}

}