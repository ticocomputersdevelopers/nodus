<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Sapin {

public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sapin/sapin_excel/sapin.xlsx');
			$products_file = "files/sapin/sapin_excel/sapin.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	       	$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 0; $row <= $lastRow; $row++) {

	            $sifra_artikla = $worksheet->getCell('F'.$row)->getValue();
	            $grupa = $worksheet->getCell('G'.$row)->getValue();
	            $podgrupa = $worksheet->getCell('W'.$row)->getValue();
	            $opis = $worksheet->getCell('H'.$row)->getValue();
	            $naziv = $worksheet->getCell('I'.$row)->getValue();
	            $slika1 = $worksheet->getCell('N'.$row)->getValue();
	            $slika2 = $worksheet->getCell('O'.$row)->getValue();
	            $slika3 = $worksheet->getCell('P'.$row)->getValue();
	            $slika4 = $worksheet->getCell('Q'.$row)->getValue();
				$cena_nc = $worksheet->getCell('V'.$row)->getValue();

				$search  = array('Å¡', 'Å¾', 'Ä','Ä‡','Ä‘');
    			$replace = array('š', 'ž', 'č','ć','đ');
    			$naziv=str_replace($search, $replace, $naziv);
    			$grupa=str_replace($search, $replace, $grupa);
    			$podgrupa=str_replace($search, $replace, $podgrupa);
    			$opis=str_replace($search, $replace, $opis);

    			$searchOpis = array('<p>','</p>');
    			$replaceOpis = array('','');
    			$opis=str_replace($searchOpis, $replaceOpis, $opis);
				if (!empty($sifra_artikla) && !empty($naziv) && !empty($cena_nc) && strpos($cena_nc,',')) {

					$search  = array(' ', ',00');
					$cena_nc=str_replace($search, '', $cena_nc);

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ", ";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra_artikla)) . "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '".pg_escape_string(Support::encodeTo1250(htmlentities(trim($grupa)))) ."',";
					if($podgrupa !== 'NULL'){
					$sPolja .= " podgrupa,";				$sVrednosti .= " '".pg_escape_string(Support::encodeTo1250(htmlentities(trim($podgrupa)))) ."',";
					}
					if(!empty($opis)){
					$sPolja .= " opis,";					$sVrednosti .= " '".pg_escape_string(Support::encodeTo1250(htmlentities(trim($opis)))) ."',";
						$sPolja .= " flag_opis_postoji,";					$sVrednosti .= "1,";
					}
					$sPolja .= " naziv,";					$sVrednosti .= " '".pg_escape_string(Support::encodeTo1250(htmlentities(trim($naziv)))) ."',";
					$sPolja .= " cena_nc, ";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ", ";
					if(!empty($slika1) or !empty($slika2) or !empty($slika3) or !empty($slika4) ){
						$sPolja .= " flag_slika_postoji,";					$sVrednosti .= "1,";
					}
					$sPolja .= " kolicina";					$sVrednosti .= "1";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
					
					if(!empty($slika1)){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra_artikla)).",'".Support::encodeTo1250($slika1)."',1 )");
					}
					if(!empty($slika2)){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra_artikla)).",'".Support::encodeTo1250($slika2)."',1 )");
					}
					if(!empty($slika3)){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra_artikla)).",'".Support::encodeTo1250($slika3)."',1 )");
					}
					if(!empty($slika4)){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".addslashes(Support::encodeTo1250($sifra_artikla)).",'".Support::encodeTo1250($slika4)."',1 )");
					}				
				}
			}
		}

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sapin/sapin_excel/sapin.xlsx');
			$products_file = "files/sapin/sapin_excel/sapin.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 0; $row <= $lastRow; $row++) {

	            $sifra_artikla = $worksheet->getCell('F'.$row)->getValue();
				$cena_nc = $worksheet->getCell('V'.$row)->getValue();

				if (!empty($sifra_artikla) && !empty($naziv) && !empty($cena_nc)) {

					$search  = array(' ', ',00');
					$cena_nc=str_replace($search, '', $cena_nc);

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra_artikla)) . "',";
					$sPolja .= " cena_nc, ";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ", ";
					$sPolja .= " kolicina";					$sVrednosti .= "1";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");						
				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
	            }				
			}
		}
	}
}