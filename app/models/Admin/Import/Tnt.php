<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Tnt {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){

			Support::autoDownload(Support::autoLink($dobavljac_id),'files/tnt/tnt_xml/tnt.xml');
			$products_file = "files/tnt/tnt_xml/tnt.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();
			$products = simplexml_load_file($products_file,'SimpleXMLElement',LIBXML_NOCDATA);	
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$sifra= (string) $product->id;
				$naziv= (string) $product->naziv;
				$model = (string) $product->model;
				$proizvodjac = (string) $product->proizvodjac;
				$slika = (string) $product->slika;
				$opis = (string) $product->opis;
				$grupa = (string) $product->$product->vrsta_naziv;
				$kolicina = (int) $product->ima_na_stanju;
				$cena_nc = (float) str_replace(",","",(string) $product->veleprodajna_cena_din);
				$mpcena = (float) str_replace(",","",(string) $product->maloprodajna_cena_din);
				

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($sifra)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($naziv)) . ",";
			$sPolja .= "model,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($model)) . ",";

			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($proizvodjac)) . ",";
			if (isset($opis) && !empty($opis)) {
				$sPolja .= "opis,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($opis)) . ",";
				$sPolja .= "flag_opis_postoji,";		$sVrednosti .= "1,";
			}
			
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->grupa)) . ",";
			
			if (isset($slika) && !empty($slika)) {
				$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "1,";
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$sifra."','".addslashes(Support::encodeTo1250($slika))."',1 )");
			}
			$sPolja .= "pdv,";						$sVrednosti .= "" . 20 . ",";
			if($kolicina != ''){
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
			}else{
			$sPolja .= "kolicina,";					$sVrednosti .= " 0,";	
			}
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena_nc . ",";
			$sPolja .= "mpcena,";					$sVrednosti .= "" . $mpcena . ",";
			$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";
			
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/tnt/tnt_xml/tnt.xml');
			$products_file = "files/tnt/tnt_xml/tnt.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

		$products = simplexml_load_file($products_file,'SimpleXMLElement',LIBXML_NOCDATA);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra= (string) $product->id;
				$kolicina = (int) $product->stanje;
				$cena_nc = (float) str_replace(",","",(string) $product->veleprodajna_cena_din);
				$mpcena = (float) str_replace(",","",(string) $product->maloprodajna_cena_din);


				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				if($kolicina != ''){
				$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
				}else{
				$sPolja .= "kolicina,";					$sVrednosti .= " 0,";	
				}
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena_nc . ",";
				$sPolja .= "mpcena,";					$sVrednosti .= "" . $mpcena . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $mpcena . "";

						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}
	
}