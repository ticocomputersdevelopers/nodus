<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Sampro {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){

			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sampro/sampro_xml/sampro.xml');
			$products_file = "files/sampro/sampro_xml/sampro.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->Id;
				$proizvodjac = (string) $product->Manufacturer;
				$naziv = (string) $product->Name;
				$grupa = (string) $product->Category;
				if ((float) $product->Price_Retail > 0) {
					$cena_nc = (float) $product->Price_Retail;
				} else {
					$cena_nc = (float) $product->Price;
				}
				$kolicina = (int) $product->Quantity;
				$slika = (string) $product->ImageUrl;
				$specifikacije = $product->Specification->string;
				$pmp = (float) $product->Price_VAT;
				$opis = "";
				foreach ($specifikacije as $specifikacija) {
									
					$opis .= (string) $specifikacija."<br>"; 
				}


				
				$sPolja .= " partner_id,";					$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";		$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
				$sPolja .= " proizvodjac,";					$sVrednosti .= " '" . Support::encodeTo1250($proizvodjac). "',";
				$sPolja .= " naziv,";						$sVrednosti .= " '" . Support::encodeTo1250($naziv). " ( " . $sifra . " )',";
				if(!empty($opis)) {
					$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "1,";
					$sPolja .= " opis,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($opis)). "',";
				}
				$sPolja .= " grupa,";						$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
				if (isset($slika) && !empty($slika)) {
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= "1,";
				}
				$sPolja .= " pmp_cena,";					$sVrednosti .= " " . Support::replace_empty_numeric($pmp,1,$kurs,$valuta_id_nc). ",";
				$sPolja .= " cena_nc,";						$sVrednosti .= " " . Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc). ",";
				$sPolja .= " kolicina";						$sVrednosti .= " " . $kolicina. "";
				
				if (DB::table('dobavljac_cenovnik_temp')->where('sifra_kod_dobavljaca',$sifra)->first()) {
	
				} else {
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					
					if (isset($slika) && !empty($slika)) {						
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$sifra."','".addslashes(Support::encodeTo1250($slika))."',1 )");
					}
					
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){

			Support::autoDownload(Support::autoLink($dobavljac_id),'files/sampro/sampro_xml/sampro.xml');
			$products_file = "files/sampro/sampro_xml/sampro.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			
		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->Id;	
				if ((float) $product->Price_Retail > 0) {
					$cena_nc = (float) $product->Price_Retail;
				} else {
					$cena_nc = (float) $product->Price;
				}
				$kolicina = (int) $product->Quantity;
				$pmp = (float) $product->Price_VAT;

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . $sifra . "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc). ",";
				$sPolja .= " pmp_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric($pmp,1,$kurs,$valuta_id_nc). ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";

				if (DB::table('dobavljac_cenovnik_temp')->where('sifra_kod_dobavljaca',$sifra)->first()) {

				} else {
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	
}