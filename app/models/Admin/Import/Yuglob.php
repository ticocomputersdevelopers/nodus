<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Yuglob {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/yuglob/yuglob_excel/yuglob.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('E'.$row)->getValue();

				$modeli1 = trim($worksheet->getCell('C'.$row)->getValue());
				$modeli2 = trim($worksheet->getCell('D'.$row)->getValue());

				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){

					$sifra_arr = array();
					$modeli_arr = array();
					if($modeli1 != '' || $modeli2 != ''){
						if($modeli1 != ''){
							self::modeli($modeli1,$modeli_arr);
						}
						if($modeli2 != ''){
							self::modeli($modeli2,$modeli_arr);
						}
						foreach($modeli_arr as $mod){
							if(self::check_submodel($sifra,$mod)){
								$sifra_arr[] = $sifra;			
							}else{
								$sifra_mod = $sifra.'.'.$mod;
								$sifra_arr[] = $sifra_mod;
							}
						}

					}else{
						$sifra_arr[0] = $sifra;
					}

					$kolicina = 1;
					if($modeli1 == '' && $modeli2 == ''){
						$kolicina = 0;
					}

					foreach($sifra_arr as $sifra_new){
						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
						$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra_new)) ."',";
						$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv)) ." ( ".addslashes(Support::encodeTo1250($sifra_new))." )',";
						$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";		
						$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $cena . ",";
						$sPolja .= "mpcena,";					$sVrednosti .= "" . $cena . ",";
						$sPolja .= "web_cena";					$sVrednosti .= "" . $cena . "";
				
						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}

				}

			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			$products_file = "files/yuglob/yuglob_excel/yuglob.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('E'.$row)->getValue();

				$modeli1 = trim($worksheet->getCell('C'.$row)->getValue());
				$modeli2 = trim($worksheet->getCell('D'.$row)->getValue());

				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){

					$sifra_arr = array();
					$modeli_arr = array();
					if($modeli1 != '' || $modeli2 != ''){
						if($modeli1 != ''){
							self::modeli($modeli1,$modeli_arr);
						}
						if($modeli2 != ''){
							self::modeli($modeli2,$modeli_arr);
						}
						foreach($modeli_arr as $mod){
							if(self::check_submodel($sifra,$mod)){
								$sifra_arr[] = $sifra;			
							}else{
								$sifra_mod = $sifra.'.'.$mod;
								$sifra_arr[] = $sifra_mod;
							}
						}

					}else{
						$sifra_arr[0] = $sifra;
					}

					$kolicina = 1;
					if($modeli1 == '' && $modeli2 == ''){
						$kolicina = 0;
					}

					foreach($sifra_arr as $sifra_new){
						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
						$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra_new)) ."',";
						$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";		
						$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $cena . ",";
						$sPolja .= "mpcena,";					$sVrednosti .= "" . $cena . ",";
						$sPolja .= "web_cena";					$sVrednosti .= "" . $cena . "";
				
						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}
			}
			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function modeli($modeli,&$modeli_arr){
		if(strlen($modeli) > 3 && strpos($modeli,',') === false && is_numeric($modeli)){
			$modeli = strval(number_format($modeli,0, '.', ','));
		}	
		$modeli_coma_arr = explode(',',$modeli);
		foreach($modeli_coma_arr as $mod_coma){
			$modeli_point_arr = explode('.',$mod_coma);
			foreach($modeli_point_arr as $mod_point){
				$modeli_arr[] = $mod_point;
			}
		}
		
	}

	public static function check_submodel($sifra,$mod){
		if(substr($sifra,-strlen($mod)) == $mod){
			return true;
		}else{
			return false;
		}
	}

}