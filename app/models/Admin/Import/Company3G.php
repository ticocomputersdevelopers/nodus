<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;
use ZipArchive;

class Company3G {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			self::autoDownload($dobavljac_id);

			$file_name = Support::file_name("files/3gcompany/");
			if($file_name!==false){
				$products = simplexml_load_file("files/3gcompany/".$file_name, 'SimpleXMLElement', LIBXML_NOCDATA);
			}else{
				$products = array();
			}
		}else{
			$file_name = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file_name, 'SimpleXMLElement', LIBXML_NOCDATA);	

		}
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			foreach ($products as $product):

				$sifra = (string) $product->id;
				$barkod = (string) $product->barKod;
				$naziv = (string) $product->naziv;
				$grupa = (string) $product->kategorija3;
				$podgrupa = (string) $product->kategorija4;
				$opis = (string) $product->opis;

				if((string) $product->dostupan == '1') {
					$kolicina = 1;
				} else {
					$kolicina = 0;
				}
				$cena_nc = (float) $product->vpCena;

				$slike = $product->slike->slika;

				$flag_slika_postoji = 0;
				foreach ((array) $slike as $key => $slika) {
					if ($key == 0) {
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',1 )");
					} else {
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',0 )");
					}
				$flag_slika_postoji = 1;
				}

				$sPolja = '';
				$sVrednosti = '';

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " " . Support::quotedStr($sifra) . ",";	
				$sPolja .= " barkod,";					$sVrednosti .= " " . Support::quotedStr($barkod) . ",";
				$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($naziv)) . "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa) . "',";
				$sPolja .= " podgrupa,";				$sVrednosti .= " '" . Support::encodeTo1250($podgrupa) . "',";
				if(isset($opis) && !empty($opis)) {
				$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis) . "',";
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "1,";
				}
				$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " '" . $flag_slika_postoji . "',";
				$sPolja .= " kolicina,";				$sVrednosti .= " '" . $kolicina . "',";
				$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";
				
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		
			
			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($file_name)){
                    File::delete($file_name);
                }				
			}
		}



	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			self::autoDownload($dobavljac_id);
			$file_name = Support::file_name("files/3gcompany/");
			if($file_name!==false){
				$products = simplexml_load_file("files/3gcompany/".$file_name, 'SimpleXMLElement', LIBXML_NOCDATA);
			}else{
				$products = array();
			}
		}else{
			$file_name = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file_name, 'SimpleXMLElement', LIBXML_NOCDATA);		
		}
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->id;
				if((string) $product->dostupan == '1') {
					$kolicina = 1;
				} else {
					$kolicina = 0;
				}
				$cena_nc = (float) $product->vpCena;


				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " " . Support::quotedStr($sifra) . ",";	
				$sPolja .= " kolicina,";				$sVrednosti .= " '" . $kolicina . "',";
				$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
											
			endforeach;

			// Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($file_name)){
                    File::delete($file_name);
                }				
			}
		}



public static function autoDownload($dobavljac_id){

		Support::autoDownload(Support::autoLink($dobavljac_id),'files/3gcompany/3gcompany.zip');

		$zip = new ZipArchive;
		if ($zip->open('files/3gcompany/3gcompany.zip') === TRUE) {
		    $zip->extractTo('files/3gcompany');
		    $zip->close();
		    File::delete('files/3gcompany/3gcompany.zip');
		}
	
}
}