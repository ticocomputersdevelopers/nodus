<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class MsSecuritySystem {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/mssecuritysystem/mssecuritysystem_excel/mssecuritysystem.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 2; $row <= $lastRow; $row++) {
	            $proizvodjac = $worksheet->getCell('A'.$row)->getValue();
	            $grupa = $worksheet->getCell('B'.$row)->getValue();
	            $sifra = $worksheet->getCell('C'.$row)->getValue();
	            $naziv = $worksheet->getCell('D'.$row)->getValue();
	            $opis = $worksheet->getCell('E'.$row)->getValue();
	            $slike =  explode(" ",$worksheet->getCell('F'.$row)->getValue());
	            $cena_nc = $worksheet->getCell('G'.$row)->getValue();
	            $kolicina = $worksheet->getCell('H'.$row)->getValue();

	            // var_dump($naziv); die;
				if(isset($sifra) && !empty($sifra) && isset($naziv) && !empty($naziv) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){

					foreach ($slike as $key => $slika) {
						if($key==0){
							DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',1 )");
						}else{
							DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',0 )");
						}
					}

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($proizvodjac)) . "',";
					if(!empty($opis)) {
						$sPolja .= " flag_opis_postoji,";	$sVrednosti .= "1,";
						$sPolja .= " opis,";				$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($opis))) . "',";
					}
					if(!empty($slike)) {
						$sPolja .= " flag_slika_postoji,";	$sVrednosti .= "1,";
					}
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina. ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	



				}


			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/mssecuritysystem/mssecuritysystem_excel/mssecuritysystem.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 2; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('C'.$row)->getValue();
				$cena_nc = $worksheet->getCell('G'.$row)->getValue();
	            $kolicina = $worksheet->getCell('H'.$row)->getValue();

				if(isset($sifra) && !empty($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0){

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina. ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,2,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

				}

			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}