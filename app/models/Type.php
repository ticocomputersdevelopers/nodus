<?php

class Type extends Eloquent
{
    protected $table = 'tip_artikla';

    protected $primaryKey = 'tip_artikla_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'tip_artikla_id',
        'naziv',
        'grupa_pr_id',
        'rbr',
        'prikaz'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['tip_artikla_id']) ? $this->attributes['tip_artikla_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
	}
    public function getShowAttribute()
    {
        return isset($this->attributes['prikaz']) ? $this->attributes['prikaz'] : null;
    }
    public function getSortAttribute()
    {
        return isset($this->attributes['rbr']) ? $this->attributes['rbr'] : null;
    }

	protected $appends = array(
    	'id',
    	'name',
        'show',
        'sort'
    	);

    public function articles(){
        return $this->hasMany('Article', 'tip_artikla_id');
    }

    public function getIdByName($name,$mapped=[]){
        if(isset($mapped[$name])){
            return $mapped[$name];
        }
        
        if(is_null($typeId = self::where('naziv',$name)->pluck('tip_artikla_id'))){
            $nextId = self::max('tip_artikla_id')+1;
            
            $type = new Type();
            $type->tip_artikla_id = $nextId;
            $type->naziv = $name;
            $type->active = 1;
            $type->save();
            $typeId = $type->tip_artikla_id;
        }

        return $typeId;
    }

    public function mappedByName(){
        $mapped = [];
        foreach(self::select('tip_artikla_id','naziv')->get() as $type){
            $mapped[$type->naziv] = $type->tip_artikla_id;
        }
        return $mapped;
    }

}